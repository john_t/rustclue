use rustclue::{Accuracy, service::{AGENT_PATH, ManagerProxy}};
use std::collections::HashMap;
use zbus::{dbus_interface, dbus_proxy, zvariant::Value};
use futures::prelude::*;

// We use the Notification Proxy for asking the user
// to authorize the application
#[dbus_proxy(
    interface = "org.freedesktop.Notifications",
    default_service = "org.freedesktop.Notifications",
    default_path = "/org/freedesktop/Notifications",
    gen_blocking = false,
    async_name = "NotificationProxy"
)]
trait Notification {
    fn notify(
        &self,
        app_name: &str,
        replaces_id: u32,
        app_icon: &str,
        summary: &str,
        body: &str,
        actions: &[&str],
        hints: HashMap<&str, Value<'_>>,
        expire_timeout: i32,
    ) -> zbus::fdo::Result<u32>;
    #[dbus_proxy(signal)]
    fn action_invoked(&self, id: u32, key: &str);
}

#[derive(Debug, Default)]
pub struct Agent {
    accuracy: Accuracy,
}

#[dbus_interface(name = "org.freedesktop.GeoClue3.Agent")]
impl Agent {
    #[dbus_interface(out_args("authorized", "allowed_accuracy_level"))]
    pub async fn authorize_app(
        &mut self,
        desktop_id: &str,
        req_accuracy_level: Accuracy,
    ) -> zbus::fdo::Result<(bool, Accuracy)> {
        let cnx = zbus::Connection::session().await?;
        let notif_proxy = NotificationProxy::new(&cnx).await?;
        notif_proxy
            .notify(
                desktop_id,
                0,
                "",
                "Location access request",
                "Agent would like you to give tihs app location access?",
                &["yes", "Yes", "no", "No"],
                HashMap::new(),
                -1,
            )
            .await?;

        let mut stream = notif_proxy.receive_action_invoked().await?;
        let action = stream.next().await.unwrap();
        let args = action.args()?;

        let res = if args.key() == &"yes" {
            (true, self.max_accuracy_level())
        } else {
            (false, Accuracy::None)
        };
        Ok(res)
    }

    #[dbus_interface(property)]
    pub fn max_accuracy_level(&self) -> Accuracy {
        self.accuracy
    }

    #[dbus_interface(property)]
    pub fn set_max_accuracy_level(&mut self, max_accuracy_level: Accuracy) {
        self.accuracy = max_accuracy_level;
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt::init();
    let cnx = zbus::ConnectionBuilder::system()?.build().await?;

    let object_server = cnx.object_server();

    tracing::info!("Starting the agent");
    let mut agent = Agent::default();
    tracing::info!("Setting agent accuracy level to exact");
    agent.set_max_accuracy_level(Accuracy::Exact);

    object_server.at(AGENT_PATH, agent).await?;

    tracing::info!("Starting Manager proxy");
    let manager = ManagerProxy::new(&cnx).await?;

    tracing::info!("Adding gnome-shell agent");
    manager.add_agent("gnome-shell").await?;

    loop {}

    Ok(())
}
