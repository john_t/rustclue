use rustclue::service::{ManagerProxy, LocationProxy};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    tracing_subscriber::fmt::init();
    let cnx = zbus::Connection::system().await?;
    //cnx.request_name("org.gnome.clocks").await?;

    tracing::info!("Starting the Manager proxy");
    let manager = ManagerProxy::new(&cnx).await?;
    tracing::info!("Creating a client");
    let client = manager.create_client().await?;
    tracing::info!("'{}' client created", client.path());
    tracing::info!("Setting DesktopId to 'epiphany'");
    client.set_desktop_id("org.gnome.clocks").await?;
    tracing::info!("Starting the client");
    client.start().await?;

    let location_path = client.location().await?;
    tracing::info!("Received location path {:#?}", location_path);
    let location = LocationProxy::builder(&cnx).path(location_path)?.build().await?;
    let latitude = location.latitude().await?;
    let longitude = location.longitude().await?;

    tracing::info!("Client received the location ({}, {})", latitude, longitude);

    loop {}

    Ok(())
}
