//! Wrapper for `org.bluez.Device1` DBus interface used by [`BluetoothSource`](crate::sources::web::BluetoothSource).

use zbus::dbus_proxy;

use crate::Result;

#[dbus_proxy(
    interface = "org.bluez.Device1",
    default_service = "org.bluez",
    gen_blocking = false,
    async_name = "DeviceProxy"
)]
pub trait Device {
    #[dbus_proxy(property)]
    fn address(&self) -> zbus::Result<String>;

    #[dbus_proxy(property, name = "Name")]
    fn panic_name(&self) -> zbus::Result<String>;

    #[dbus_proxy(property, name = "RSSI")]
    fn panic_signal(&self) -> zbus::Result<i16>;
}

impl<'a> DeviceProxy<'a> {
    // Bluez properties tend to dissapear (no joke) for whatever reasons
    // so let us provide a safer wrapper around them by converting a potential Zbus(FDO(InvalidArgs("No such property 'Name'")))
    // to a None
    pub async fn name(&self) -> zbus::Result<Option<String>> {
        match self.panic_name().await {
            Ok(name) => Ok(Some(name)),
            Err(zbus::Error::FDO(_)) => Ok(None),
            Err(e) => Err(e),
        }
    }

    pub async fn signal(&self) -> zbus::Result<Option<i16>> {
        match self.panic_signal().await {
            Ok(signal) => Ok(Some(signal)),
            Err(zbus::Error::FDO(_)) => Ok(None),
            Err(e) => Err(e),
        }
    }

    pub async fn should_ignore(&self) -> Result<bool> {
        let address = self.address().await?;
        if address.is_empty() {
            tracing::debug!("Ignoring Bluetooth with unknown address");
            return Ok(true);
        }
        let name = self.name().await?;
        if name.map(|n| n.ends_with("_nomap")).unwrap_or(false) {
            tracing::debug!("Ignoring Bluetooth that has '_nomap' suffix");
            return Ok(true);
        }
        Ok(false)
    }
}
