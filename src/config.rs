use std::path::{Path, PathBuf};

use ini::Ini;
use once_cell::sync::Lazy;
use reqwest::Url;

use crate::{service::UserId, Error, Result};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ApplicationPermission {
    Allowed,
    Disallowed,
    AskAgent,
}

// TODO: don't hardcode `/etc` use `sysconfdir` from meson in the future instead
static CONFIG_FILE: &str = "/etc/geoclue/geoclue.conf";
static DEFAULT_KEY: &str = "geoclue";
static KNOWN_CONFIG_SECTIONS: [&str; 7] = [
    "agent",
    "network-nmea",
    "3g",
    "cdma",
    "modem-gps",
    "wifi",
    "compass",
];

pub static CONFIG: Lazy<Config> =
    Lazy::new(|| Config::from_file(CONFIG_FILE).expect("Failed to parse config file"));

#[derive(Debug, PartialEq, Eq)]
pub struct AppConfig {
    desktop_id: String,
    allowed: bool,
    system: bool,
    /// Allow all users if empty
    users: Vec<UserId>,
}

impl AppConfig {
    pub fn new(desktop_id: &str) -> Self {
        Self {
            desktop_id: desktop_id.to_owned(),
            allowed: false,
            system: false,
            users: Default::default(),
        }
    }

    pub fn set_is_allowed(mut self, is_allowed: bool) -> Self {
        self.allowed = is_allowed;
        self
    }

    pub fn set_is_system(mut self, set_is_system: bool) -> Self {
        self.system = set_is_system;
        self
    }

    pub fn add_user(&mut self, user_id: UserId) {
        self.users.push(user_id);
    }

    pub fn desktop_id(&self) -> &str {
        &self.desktop_id
    }

    pub fn is_allowed(&self) -> bool {
        self.allowed
    }

    pub fn is_system(&self) -> bool {
        self.system
    }

    pub fn users(&self) -> &[UserId] {
        &self.users
    }
}
/// A wrapper around the GeoClue configuration file.
pub struct Config {
    // List of allowed desktop ids
    agents: Vec<String>,
    config: Ini,
    is_3g_source_enabled: bool,
    is_cdma_source_enabled: bool,
    is_compass_enabled: bool,
    is_modem_gps_source_enabled: bool,
    is_nmea_source_enabled: bool,
    is_wifi_source_enabled: bool,
    url: Url,
    submit_data: bool,
    submission_url: Url,
    submission_nick: Option<String>,
    app_configs: Vec<AppConfig>,
    nmea_socket: PathBuf,
}

impl Config {
    pub fn from_file(file: impl AsRef<Path>) -> Result<Self> {
        let config =
            Ini::load_from_file(file).map_err(|_| Error::IO("Failed to parse the config file"))?;

        let mut config = Self {
            agents: Default::default(),
            is_nmea_source_enabled: false,
            is_3g_source_enabled: false,
            is_cdma_source_enabled: false,
            is_modem_gps_source_enabled: false,
            is_wifi_source_enabled: false,
            is_compass_enabled: false,
            submit_data: false,
            url: Url::parse(&format!(
                "https://www.googleapis.com/geolocation/v1/geolocate?key={}",
                DEFAULT_KEY
            ))
            .unwrap(),
            submission_url: Url::parse(&format!(
                "https://location.services.mozilla.com/v1/submit?key={}",
                DEFAULT_KEY
            ))
            .unwrap(),
            submission_nick: None,
            app_configs: Default::default(),
            nmea_socket: PathBuf::from("/var/run/gps-share.sock"),
            config,
        };
        config.parse()?;
        Ok(config)
    }

    pub fn agents(&self) -> &[String] {
        &self.agents
    }

    pub fn app_configs(&self) -> &[AppConfig] {
        &self.app_configs
    }

    pub fn is_3g_source_enabled(&self) -> bool {
        self.is_3g_source_enabled
    }

    pub fn is_cdma_source_enabled(&self) -> bool {
        self.is_cdma_source_enabled
    }

    pub fn is_compass_enabled(&self) -> bool {
        self.is_compass_enabled
    }

    pub fn is_modem_gps_source_enabled(&self) -> bool {
        self.is_modem_gps_source_enabled
    }

    pub fn is_nmea_source_enabled(&self) -> bool {
        self.is_nmea_source_enabled
    }

    pub fn is_wifi_source_enabled(&self) -> bool {
        self.is_wifi_source_enabled
    }

    pub fn is_submit_data_enabled(&self) -> bool {
        self.submit_data
    }

    pub fn nmea_socket(&self) -> &PathBuf {
        &self.nmea_socket
    }

    pub fn url(&self) -> &Url {
        &self.url
    }

    pub fn submission_url(&self) -> &Url {
        &self.submission_url
    }

    pub fn submission_nick(&self) -> &str {
        self.submission_nick.as_deref().unwrap_or("geoclue")
    }

    pub fn is_agent_allowed(&self, desktop_id: &str) -> bool {
        self.agents.iter().any(|agent| agent == desktop_id)
    }

    pub fn is_system_component(&self, desktop_id: &str) -> bool {
        self.app_configs
            .iter()
            .find(|app| app.desktop_id() == desktop_id)
            .map(|app| app.is_system())
            .unwrap_or(false)
    }

    pub fn application_permission(
        &self,
        desktop_id: &str,
        user_id: UserId,
    ) -> ApplicationPermission {
        let app_config = self
            .app_configs()
            .iter()
            .find(|app| app.desktop_id() == desktop_id);
        if app_config.is_none() {
            tracing::debug!("{desktop_id} not in configuration");
            return ApplicationPermission::AskAgent;
        }
        let app_config = app_config.unwrap();
        if !app_config.is_allowed() {
            tracing::debug!("{desktop_id} is not allowed by configuration");
            return ApplicationPermission::Disallowed;
        }
        // If there are no per users filter for the `AppConfig`
        if app_config.users().is_empty() {
            tracing::debug!("No users filter for {desktop_id}");
            return ApplicationPermission::Allowed;
        }

        if app_config.users().iter().any(|u| u == &user_id) {
            ApplicationPermission::Allowed
        } else {
            ApplicationPermission::Disallowed
        }
    }

    fn parse(&mut self) -> Result<()> {
        let agent = self.config.section(Some("agent")).ok_or(Error::Config(
            "Config file doesn't contain an agent section",
        ))?;

        let mut whitelist = agent
            .get("whitelist")
            .ok_or({
                Error::Config("Config file agent section doesn't contain a whitelist entry")
            })?
            .split(';')
            .map(|w| w.to_owned())
            .collect::<Vec<_>>();
        self.agents.append(&mut whitelist);

        tracing::debug!("Allowed agents: {:#?}", self.agents);

        self.is_3g_source_enabled = self.parse_source_config("3g")?;
        tracing::debug!("3G Source: {}", self.is_3g_source_enabled);
        self.is_cdma_source_enabled = self.parse_source_config("cdma")?;
        tracing::debug!("CDMA Source: {}", self.is_cdma_source_enabled);
        self.is_modem_gps_source_enabled = self.parse_source_config("modem-gps")?;
        tracing::debug!("Modem GPS Source: {}", self.is_modem_gps_source_enabled);
        match self.parse_source_config("compass") {
            Ok(is_enabled) => {
                self.is_compass_enabled = is_enabled;
                tracing::debug!("Compass Source: {}", self.is_compass_enabled);
            }
            Err(_err) => tracing::debug!("Config file doesn't have a compass section"),
        };

        self.parse_wifi_config()?;
        self.parse_nmea_config()?;
        self.parse_app_configs()?;
        Ok(())
    }

    fn parse_source_config(&self, cfg_section: &'static str) -> Result<bool> {
        let section = self.config.section(Some(cfg_section)).ok_or(Error::Config(
            "Config file doesn't contain an {cfg_section} section",
        ))?;

        let enabled = section.get("enable").ok_or({
            Error::Config("Config file {cfg_section} section doesn't contain an 'enable' entry")
        })? == "true";
        Ok(enabled)
    }

    fn parse_nmea_config(&mut self) -> Result<()> {
        let section = self
            .config
            .section(Some("network-nmea"))
            .ok_or(Error::Config(
                "Config file doesn't contain a 'network-nmea' section",
            ))?;

        self.is_nmea_source_enabled = section.get("enable").ok_or({
            Error::Config("Config file 'network-nmea' section doesn't contain an 'enable' entry")
        })? == "true";
        tracing::debug!("NMEA Source: {}", self.is_nmea_source_enabled);

        if let Some(nmea_socket) = section.get("nmea-socket") {
            self.nmea_socket = PathBuf::from(nmea_socket);
        }
        Ok(())
    }

    fn parse_wifi_config(&mut self) -> Result<()> {
        let section = self
            .config
            .section(Some("wifi"))
            .ok_or(Error::Config("Config file doesn't contain an wifi section"))?;

        self.is_wifi_source_enabled = section.get("enable").ok_or({
            Error::Config("Config file 'wifi' section doesn't contain an 'enable' entry")
        })? == "true";
        tracing::debug!("WiFi Source: {}", self.is_wifi_source_enabled);

        if let Some(url) = section.get("url") {
            self.url = Url::parse(url)
                .map_err(|_| Error::Config("Config file 'wifi' has an invalid 'url' entry"))?;
        }
        if let Some(submit_data) = section.get("submit-data") {
            self.submit_data = submit_data == "true"
        }

        if let Some(submission_url) = section.get("submission-url") {
            self.submission_url = Url::parse(submission_url).map_err(|_| {
                Error::Config("Config file 'wifi' has an invalid 'submission-url' entry")
            })?;
        }

        if let Some(submission_nick) = section.get("submission-nick") {
            self.submission_nick = Some(submission_nick.to_owned());
        }
        Ok(())
    }

    fn parse_app_configs(&mut self) -> Result<()> {
        for (section_name, section) in self.config.iter() {
            if section_name
                .map(|n| KNOWN_CONFIG_SECTIONS.contains(&n))
                .unwrap_or(false)
            {
                continue;
            }

            let desktop_id = section_name.unwrap(); // Assume there is a section name for app configs
            let allowed = section.get("allowed").map(|s| s == "true").unwrap_or(false);
            let system = section.get("system").map(|s| s == "true").unwrap_or(false);

            let users = section
                .get("users")
                .map(|users| {
                    if users.is_empty() {
                        vec![]
                    } else {
                        users
                            .split(';')
                            .map(|s| s.parse::<u32>().expect("Failed to parse user ID"))
                            .collect::<Vec<_>>()
                    }
                })
                .unwrap_or_default();
            self.app_configs.push(AppConfig {
                allowed,
                system,
                desktop_id: desktop_id.to_owned(),
                users,
            })
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::config::AppConfig;

    use super::{Config, PathBuf, Url};

    #[test]
    fn parse_config_file() {
        let cfg = Config::from_file("./src/tests/geoclue.conf").unwrap();

        assert_eq!(
            cfg.agents(),
            &[
                "gnome-shell".to_owned(),
                "io.elementary.desktop.agent-geoclue2".to_owned(),
                "sm.puri.Phosh".to_owned(),
                "lipstick".to_owned()
            ]
        );

        assert_eq!(cfg.is_3g_source_enabled(), true);
        assert_eq!(cfg.is_cdma_source_enabled(), true);
        assert_eq!(cfg.is_compass_enabled(), true);
        assert_eq!(cfg.is_modem_gps_source_enabled(), true);
        assert_eq!(cfg.is_nmea_source_enabled(), true);
        assert_eq!(cfg.is_wifi_source_enabled(), true);
        assert_eq!(cfg.is_submit_data_enabled(), false);
        assert_eq!(cfg.submission_nick(), "geoclue");
        assert_eq!(
            cfg.url(),
            // default value
            &Url::parse("https://www.googleapis.com/geolocation/v1/geolocate?key=geoclue").unwrap()
        );
        assert_eq!(
            cfg.submission_url(),
            // default value
            &Url::parse("https://location.services.mozilla.com/v1/submit?key=geoclue").unwrap()
        );
        assert_eq!(cfg.nmea_socket(), &PathBuf::from("/var/run/gps-share.sock"));

        assert_eq!(
            cfg.app_configs().to_owned(),
            vec![
                AppConfig::new("gnome-datetime-panel")
                    .set_is_allowed(true)
                    .set_is_system(true),
                AppConfig::new("gnome-color-panel")
                    .set_is_allowed(true)
                    .set_is_system(true),
                AppConfig::new("org.gnome.Shell")
                    .set_is_allowed(true)
                    .set_is_system(true),
                AppConfig::new("io.elementary.desktop.agent-geoclue2")
                    .set_is_allowed(true)
                    .set_is_system(true),
                AppConfig::new("sm.puri.Phosh")
                    .set_is_allowed(true)
                    .set_is_system(true),
                AppConfig::new("epiphany").set_is_allowed(true),
                AppConfig::new("firefox").set_is_allowed(true),
                AppConfig::new("lipstick")
                    .set_is_allowed(true)
                    .set_is_system(true),
            ]
        )
    }
}
