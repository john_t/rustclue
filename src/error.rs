use std::fmt;

/// Alias for [`Result`](std::result::Result) with the error type [`Error`].
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
/// The error type for GeoClue.
pub enum Error {
    Http(reqwest::Error),
    Zbus(zbus::Error),
    Config(&'static str),
    IO(&'static str),
    /// A Mozilla Location service error, consiting of the error code and the message
    LocationService(u16, String),
}

impl std::error::Error for Error {}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Http(e) => writeln!(f, "HTTP Error {}", e),
            Self::Zbus(e) => writeln!(f, "ZBus Error {}", e),
            Self::Config(e) => writeln!(f, "Config Error {}", e),
            Self::IO(e) => writeln!(f, "IO Error {}", e),
            Self::LocationService(code, message) => {
                writeln!(
                    f,
                    "Failed to fetch the location with error code {} and message {}",
                    code, message
                )
            }
        }
    }
}

impl From<zbus::Error> for Error {
    fn from(e: zbus::Error) -> Self {
        Self::Zbus(e)
    }
}

impl From<zbus::fdo::Error> for Error {
    fn from(e: zbus::fdo::Error) -> Self {
        Self::Zbus(zbus::Error::FDO(Box::new(e)))
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Self::Http(e)
    }
}
