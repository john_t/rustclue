use service::{Manager, MANAGER_DESTINATION, MANAGER_PATH};
use tracing_subscriber::{prelude::*, FmtSubscriber};
mod accuracy;
mod config;
mod error;
mod interfaces;
mod location;
mod locator;
mod service;
mod sources;

pub use error::{Error, Result};

#[tokio::main]
async fn main() -> Result<()> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(tracing::Level::DEBUG)
        .finish();
    subscriber.init();

    let cnx = zbus::ConnectionBuilder::system()?
        .name(MANAGER_DESTINATION)?
        .build()
        .await?;

    let manager = Manager::new(&cnx).await?;

    let object_server = cnx.object_server();
    object_server.at(MANAGER_PATH, manager).await?;

    loop {}
    Ok(())
}
