use zbus::dbus_proxy;

use super::ClientProxy;
use crate::accuracy::Accuracy;

#[dbus_proxy(
    interface = "org.freedesktop.GeoClue3.Manager",
    default_service = "org.freedesktop.GeoClue3",
    default_path = "/org/freedesktop/GeoClue3/Manager",
    gen_blocking = false,
    async_name = "ManagerProxy"
)]
pub trait Manager {
    #[dbus_proxy(property)]
    fn in_use(&self) -> zbus::fdo::Result<bool>;

    #[dbus_proxy(property)]
    fn available_accuracy_level(&self) -> zbus::fdo::Result<Accuracy>;

    #[dbus_proxy(object = "Client")]
    fn get_client(&self);

    #[dbus_proxy(object = "Client")]
    fn create_client(&self);

    fn delete_client(&self, client: &ClientProxy<'_>) -> zbus::fdo::Result<()>;

    fn add_agent(&self, id: &str) -> zbus::fdo::Result<()>;
}
