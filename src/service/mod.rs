//! The GeoClue DBus service, composed of a [`Manager`] that allows you
//! get access to a [`Client`].
//!
//! The client is then used to set the various properties for the location you
//! would like to receive, e.g time/distance threshold for example.
//!  
//! Then whenever the location is updated, the client would emit a `location-updated`
//! signal that you can listen to and get the [`Location`] object path.
//!
//! With a [`Location`] object you can then retrieve the details of the geolocation
//! such as the latitude/longitude.

#[allow(unused)]
/// The agent object path.
pub const AGENT_PATH: &str = "/org/freedesktop/GeoClue3/Agent";
#[allow(unused)]
/// The agent service name.
pub const AGENT_DESTINATION: &str = "org.freedesktop.GeoClue3";
#[allow(unused)]
/// The agent interface name.
pub const AGENT_INTERFACE: &str = "org.freedesktop.GeoClue3.Agent";

#[allow(unused)]
/// The client object path.
pub const CLIENT_PATH: &str = "/org/freedesktop/GeoClue3/Client";
#[allow(unused)]
/// The client service name.
pub const CLIENT_DESTINATION: &str = "org.freedesktop.GeoClue3";
#[allow(unused)]
/// The client interface name.
pub const CLIENT_INTERFACE: &str = "org.freedesktop.GeoClue3.Client";

#[allow(unused)]
/// The manager object path.
pub const MANAGER_PATH: &str = "/org/freedesktop/GeoClue3/Manager";
#[allow(unused)]
/// The manager service name.
pub const MANAGER_DESTINATION: &str = "org.freedesktop.GeoClue3";
#[allow(unused)]
/// The manager interface name.
pub const MANAGER_INTERFACE: &str = "org.freedesktop.GeoClue3.Manager";

#[allow(unused)]
/// The location object path.
pub const LOCATION_PATH: &str = "/org/freedesktop/GeoClue3/Location";
#[allow(unused)]
/// The location service name.
pub const LOCATION_DESTINATION: &str = "org.freedesktop.GeoClue3";
#[allow(unused)]
/// The location interface name.
pub const LOCATION_INTERFACE: &str = "org.freedesktop.GeoClue3.Location";

mod agent_proxy;
mod client;
mod client_info;
mod client_proxy;
mod location;
mod location_proxy;
mod manager;
mod manager_proxy;

pub use agent_proxy::AgentProxy;
pub use client::{Client, UserId};
pub use client_proxy::ClientProxy;
pub use location::Location;
pub use location_proxy::LocationProxy;
pub use manager::Manager;
pub use manager_proxy::ManagerProxy;
