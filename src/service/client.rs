use std::sync::Arc;

use event_listener::{Event, EventListener};
use futures::lock::Mutex;
use serde::Serialize;
use zbus::{
    dbus_interface,
    zvariant::{ObjectPath, OwnedObjectPath, Type},
    SignalContext,
};

use crate::{
    accuracy::Accuracy,
    config::{ApplicationPermission, CONFIG},
    location::Location,
    locator::Locator,
};

use super::{client_info::ClientInfo, AgentProxy, LOCATION_PATH};

pub type UserId = u32;

#[derive(Debug, Clone, Type)]
#[zvariant(signature = "o")]
pub struct Client {
    client_info: Arc<ClientInfo>,
    path: ObjectPath<'static>,
    agent: AgentProxy<'static>,
    locator: Arc<Mutex<Option<Locator>>>,
    desktop_id: Option<String>,
    accuracy: Accuracy,
    time_threshold: u32,
    distance_threshold: u32,
    active: Arc<Mutex<bool>>,
    active_event: Arc<Event>,
    current_location: Option<Location>,
    location_path: OwnedObjectPath,
    n_location_updates: u32,
    cnx: zbus::Connection,
}

impl Client {
    pub async fn new(
        cnx: &zbus::Connection,
        client_info: ClientInfo,
        path: ObjectPath<'static>,
        agent: AgentProxy<'static>,
    ) -> Self {
        Self {
            client_info: Arc::new(client_info),
            path,
            agent,
            locator: Default::default(),
            desktop_id: None,
            accuracy: Default::default(),
            distance_threshold: 0,
            time_threshold: 0,
            active: Arc::new(Mutex::new(false)),
            active_event: Default::default(),
            current_location: None,
            n_location_updates: 0,
            cnx: cnx.clone(),
            location_path: OwnedObjectPath::default(),
        }
    }

    pub fn info(&self) -> &ClientInfo {
        &self.client_info
    }

    pub fn path(&self) -> &ObjectPath<'_> {
        &self.path
    }

    pub fn active_updated(&self) -> EventListener {
        self.active_event.listen()
    }

    pub async fn set_is_active(&mut self, active: bool) {
        *self.active.lock().await = active;
        tracing::debug!("Client active status changed to {}", active);
        self.active_changed(&zbus::SignalContext::new(&self.cnx, self.path()).unwrap())
            .await;
        self.active_event.notify(usize::MAX);
    }

    /// Generate a [`LocationProxy`] path for the next location update
    fn location_path(&mut self) -> OwnedObjectPath {
        self.n_location_updates += 1;
        let path = format!("{}/{}", LOCATION_PATH, self.n_location_updates);
        // can't fail
        OwnedObjectPath::try_from(path).unwrap()
    }

    async fn on_location_updated(&mut self) -> zbus::Result<()> {
        tracing::debug!("Client location updated, exporting the new interface");
        // TODO: check for the thresholds
        let path = self.location_path();
        // TODO: add a timeout to drop the old location path

        // Assume there is always a location when this is triggered
        let current_location = self.current_location.take().unwrap();

        let location_iface = crate::service::Location::new(current_location);
        self.set_location(path.clone());

        let object_server = self.cnx.object_server();
        object_server.at(&path, location_iface).await?;
        Ok(())
    }

    fn set_location(&mut self, path: OwnedObjectPath) {
        self.location_path = path;
    }
}

#[dbus_interface(name = "org.freedesktop.GeoClue3.Client")]
impl Client {
    #[dbus_interface(property)]
    pub fn location(&self) -> ObjectPath<'_> {
        self.location_path.as_ref()
    }

    #[dbus_interface(property)]
    pub fn distance_threshold(&self) -> u32 {
        self.distance_threshold
    }

    #[dbus_interface(property)]
    pub fn set_distance_threshold(&mut self, distance_threshold: u32) {
        self.distance_threshold = distance_threshold;
    }

    #[dbus_interface(property)]
    pub fn time_threshold(&self) -> u32 {
        self.time_threshold
    }

    #[dbus_interface(property)]
    pub fn set_time_threshold(&mut self, time_threshold: u32) {
        self.time_threshold = time_threshold;
    }

    #[dbus_interface(property)]
    pub fn desktop_id(&self) -> &str {
        self.desktop_id.as_deref().unwrap_or("")
    }

    #[dbus_interface(property)]
    pub fn set_desktop_id(&mut self, desktop_id: &str) {
        self.desktop_id = Some(desktop_id.to_owned());
    }

    #[dbus_interface(property)]
    pub fn requested_accuracy_level(&self) -> Accuracy {
        self.accuracy
    }

    #[dbus_interface(property)]
    pub fn set_requested_accuracy_level(&mut self, requested_accuracy_level: Accuracy) {
        self.accuracy = requested_accuracy_level;
    }

    #[dbus_interface(property)]
    pub async fn active(&self) -> bool {
        *self.active.lock().await
    }

    pub async fn start(
        &mut self,
        #[zbus(connection)] cnx: &zbus::Connection,
    ) -> zbus::fdo::Result<()> {
        // Client is already started
        if self.locator.lock().await.is_some() {
            return Ok(());
        }
        tracing::debug!("Client not started yet, starting...");

        let xdg_id = self.info().xdg_id();

        let desktop_id = if let Some(xdg_id) = xdg_id {
            Ok(xdg_id)
        } else if !self.desktop_id().is_empty() {
            Ok(self.desktop_id())
        } else {
            Err(zbus::fdo::Error::Failed(
                "'DesktopId' property must be set".to_owned(),
            ))
        }?;

        let user_id = self.info().user_id();
        let app_perm = CONFIG.application_permission(desktop_id, user_id);
        if app_perm == ApplicationPermission::Disallowed {
            tracing::debug!("Application '{desktop_id}' is not allowed per the configration");
            return Err(zbus::fdo::Error::Failed(format!(
                "{desktop_id} is disallowed by configuration for {user_id}"
            )));
        }
        // TODO: check if the agent doesn't exist yet (because the client was created earlier)
        // and do a timeout to re-try again
        let max_accuracy = self.agent.max_accuracy_level().await?;
        let requested_accuracy = self.requested_accuracy_level();

        if max_accuracy == Accuracy::None {
            return Err(zbus::fdo::Error::Failed(format!(
                "Geolocation is disabled for {user_id}"
            )));
        }

        // System components have an empty xdg-id and we can't reliably identify them
        // no need for an auth in that case
        if !(app_perm == ApplicationPermission::Allowed && xdg_id.is_none()) {
            let (authorized, accuracy) = self
                .agent
                .authorize_app(desktop_id, requested_accuracy)
                .await?;
            if !authorized {
                return Err(zbus::fdo::Error::Failed(format!("
                    Agent rejected '{desktop_id}' for user '{user_id}'. 
                    Please ensure that '{desktop_id}' has installed a valid {desktop_id}.desktop file
                ")));
            }
        }

        match Locator::new(cnx).await {
            Ok(mut locator) => {
                tracing::debug!("'{desktop_id}' client started");
                self.set_is_active(true).await;
                // TODO: set accuracy/time threshold on locator
                locator.start().await.map_err(|err| {
                    zbus::fdo::Error::Failed(format!("Failed to start location provider {err}"))
                })?;
                let event_listener = locator.location_updated();
                locator.refresh().await;
                event_listener.await;
                // Wait for a new location
                self.current_location = locator.location();
                self.on_location_updated().await;

                self.locator.lock().await.replace(locator);
                Ok(())
            }
            Err(err) => Err(zbus::fdo::Error::Failed(format!(
                "Failed to start location provider {err}"
            ))),
        }
    }

    pub async fn stop(&mut self) -> zbus::fdo::Result<()> {
        if let Some(locator) = self.locator.lock().await.take() {
            locator.stop().await.map_err(|err| {
                zbus::fdo::Error::Failed(format!("Failed to stop the client {err}"))
            })?;
        }
        self.set_is_active(false).await;
        Ok(())
    }

    #[dbus_interface(signal)]
    pub async fn location_updated(
        signal_ctxt: &SignalContext<'_>,
        old: ObjectPath<'_>,
        new: ObjectPath<'_>,
    ) -> zbus::Result<()>;
}

impl Serialize for Client {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.path())
    }
}
