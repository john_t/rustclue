//! The various [`Location`](crate::location::Location) sources.
//!
//! GeoClue makes use of various sources if available:
//!  - [Web](crate::sources::web): Sources that makes use of a geolocation service like [Mozilla Location Service](https://location.services.mozilla.com/).
//!  - GPS
pub mod web;
